import os
import sys
import json
import subprocess
import string
from ast import literal_eval
import cv2

imgFilesRootDir  = 'faces'
outputRootDir    = 'trainingInput'


'''
Each line of textFilesRootDir is structured as follows

   imgDirName        imageName                 face_id  agerange gender  x   y   dx  dy  tilt_ang    fiducial_yaw_angle  fiducial_score
115321157@N03   12111738395_a7f715aa4e_o.jpg    1744    (4, 6)     m    663 997 637 638 -95                0                  129

These fields on each line are tab separated. 

The line above contains details about the imagefile imgDirName/imagName. 
This script goes through all the text files in textFilesRootDir
It extracts the age and gender, and uses them to determine which target directory that image file shoudl go to
It creates a shell script that is executed right at the end to move all the files to the respective target dir
'''

def getOutDirName(ageRange, gender):
    return 'images_' + str(ageRange[0]) + 'to' + str(ageRange[1]) + '_' + gender

def getImgGenderFromLabel(genderLabel):
    if genderLabel.lower() == 'm':
        return 'male'
    elif genderLabel.lower() == 'f':
        return 'female'
    elif genderLabel.lower() == 'u':
        return 'unknown'
    else:
        return None

def handleError(printStr, splitLine, inputFileFullName):
    print printStr + "   " + str.join(',,', splitLine[:-1])
    '''
    frame = cv2.imread(inputFileFullName)
    cv2.imshow('frame', frame)
    cv2.waitKey(0)
    '''

def main(argv):
    ageRanges = [[0, 12], [13, 25], [25, 53], [54, 100]]
    genders   = ['male', 'female', 'unknown']
    genderLabels = ['m', 'f', 'u']

    if os.path.exists(outputRootDir):
        print outputRootDir + "exists"
        pass
    else:
        os.mkdir(outputRootDir)
        print outputRootDir + " created"
    outputFullDirNames = []
    for ageRange in ageRanges:
        for gender in genders:
            outDirName = getOutDirName(ageRange, gender)
            outputFullDirName = os.path.join(outputRootDir, outDirName)
            outputFullDirNames.append(outputFullDirName)
            if os.path.exists(outputFullDirName):
                print outputFullDirName + " exists"
            else:
                os.mkdir(outputFullDirName) 
                print outputFullDirName + " created"

    if argv:
        imgOption = 'NEW'
        textFileNames = ['wiki_face_params.txt']
    else:
        imgOption = 'ORIG'
        textFilesRootDir = 'TrainImgList'
        textFileNames = [os.path.join(textFilesRootDir, textFileName) for textFileName in os.listdir(textFilesRootDir)]

    shellFile = open('createLabels.sh', 'w')
    for textFileNameWithPath in textFileNames:
        f1 = open(textFileNameWithPath, 'r')
        f1Lines = f1.readlines()
        f1Lines = f1Lines[1:]
        print "-------  " + textFileNameWithPath + "  -------"
        for f1Line in f1Lines:
            splitLine       = f1Line.split('\t')
            if imgOption == 'ORIG':
            
                if len(splitLine) < 5:
                    # ill formed lined
                    print "Ignoring ill formed line : " + str.join('. ', splitLine[:-1])
                    continue
                imgDirName      = splitLine[0]
                imgFileName     = 'coarse_tilt_aligned_face.' + splitLine[2] + '.' + splitLine[1]
                inputFileFullName = os.path.join(imgFilesRootDir, imgDirName, imgFileName)
                if os.path.exists(inputFileFullName) is False:
                    print "Error : " + inputFileFullName + " does not exist. Ignoring" 
                    continue
            
                ageField = literal_eval(splitLine[3])
                if type(ageField) is tuple:
                    (inputMinAge, inputMaxAge) = ageField
                elif type(ageField) is int:
                    inputMinAge = ageField
                    inputMaxAge = ageField
                else:
                    handleError("Unexpected age field, ignoring", splitLine, inputFileFullName)
                    print ageField
                    continue
                imgGenderLabel  = splitLine[4]
                imgGender       = getImgGenderFromLabel(imgGenderLabel)
           
                if imgGender is None:
                    handleError("Unexpected gender label, ignoring ", splitLine, inputFileFullName)
                    continue

            elif imgOption == 'NEW':
                inputFileFullName = splitLine[0]
                
                inputMinAge = int(splitLine[1]) 
                inputMaxAge = inputMinAge 

                imgGenderCode = splitLine[2][0]
                if imgGenderCode == '0':
                    imgGenderLabel = 'm'
                    imgGender      = getImgGenderFromLabel(imgGenderLabel)
                elif imgGenderCode ==  '1':
                    imgGenderLabel = 'f'
                    imgGender      = getImgGenderFromLabel(imgGenderLabel)
                else:
                    print imgGenderCode
                    imgGenderLabel = 'u'
                    handleError("Unexpected gender label, ignoring ", splitLine, inputFileFullName)
                    continue

                if os.path.exists(inputFileFullName) is False:
                    print "Error: " + inputFileFullName + " does not exist. Ignoring"
                    continue
                else:
                    splitFileName = inputFileFullName.split('/')
                    imgFileName   = splitFileName[1]
                    #print imgFileName

            # Get output file name
            ageRangeFound = False
            for ageRange in ageRanges:
                if (inputMinAge >= ageRange[0]) and (inputMaxAge <= ageRange[1]):
                    outDirName = getOutDirName(ageRange, imgGender)
                    #print splitLine[3] + ', ' + splitLine[4] + ' -> ' + outDirName
                    ageRangeFound = True
                    break
            if ageRangeFound:
                outputFullFileName = os.path.join(outputRootDir, outDirName, imgFileName)
            else:
                handleError("Age range not found, ignoring ", splitLine, inputFileFullName)
                continue
            
            # Write command to shell file
            if os.path.exists(outputFullFileName):
                #print outputFullFileName + " already exits"
                pass
            else:
                cpCommand = 'cp ' + inputFileFullName + ' ' + outputFullFileName + '\n'
                shellFile.write(cpCommand)
        f1.close()
    shellFile.close()

if __name__ == '__main__': 
    main(sys.argv[1:])

