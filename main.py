#-------------------------------------------------------------------------------
# Name:        Object bounding box label tool
# Purpose:     Label object bboxes for ImageNet Detection data
# Author:      Qiushi
# Created:     06/06/2014

#
#-------------------------------------------------------------------------------
from __future__ import division
try:
    # for Python2
    from Tkinter import *   ## notice capitalized T in Tkinter 
except ImportError:
    # for Python3
    from tkinter import *
##import tkMessageBox
import tkinter.messagebox
#from PIL import Image, ImageTk
from PIL import Image, ImageTk
from tkinter import ttk
import os
import glob
import random
import cv2
import shutil

# colors for the bboxes
COLORS = ['red', 'blue', 'olive', 'teal', 'cyan', 'green', 'black']
# image sizes for the examples
SIZE = 256, 256

def extractFramesFromFile(sourceFileName):
    rawSourceFileName = os.path.split(sourceFileName)[-1].split('.')[0]
    print rawSourceFileName

    baseDirName = rawSourceFileName
    if os.path.exists(baseDirName):
        pass
    else:
        print "Creating base dir " + baseDirName
        os.mkdir(baseDirName) 
  
    targetDirName = os.path.join(baseDirName, 'Images')
    if os.path.exists(targetDirName):
        pass
    else:
        print "Creating target dir " + targetDirName
        os.mkdir(targetDirName)
  
 
    cap = cv2.VideoCapture(sourceFileName)
    

    numInputFrames   = cap.get(7)
    inputFrameRate   = cap.get(5)
    targetFrameRate  = 2              # 2 fps is plenty
    downSamplingRate = max(int(inputFrameRate / targetFrameRate), 1)
    numOutputFrames  = int(numInputFrames / downSamplingRate) 
    
    if numOutputFrames > 30:
        interactiveFlag = True
    else:
        interactiveFlag = False
   
    print "numInputFrames = " + str(numInputFrames) + ", inputFrameRate = " + str(inputFrameRate) 
    
    frameNumber = 0
    numFilesWritten = 0
    continueFlag = True
    while cap.isOpened() and continueFlag:
        ret, frame = cap.read()
        if ret:
            frameNumber= frameNumber + 1
            if frameNumber%downSamplingRate == 0:
                targetFileName = os.path.join(targetDirName, rawSourceFileName +  '_frame' + str(frameNumber) + '.jpg')
                if interactiveFlag:
                    cv2.imshow('Press y(n) to save (discard) frame_' + str(frameNumber), frame)
                    userInput = cv2.waitKey(0) & 0xff 
                    if userInput == ord('q'):
                        continueFlag = False
                    elif userInput == ord('y'):
                        saveFlag = True
                        cv2.imwrite(targetFileName, frame)
                        print "Wrote frame number "   + str(frameNumber) + " to " + targetFileName
                        numFilesWritten = numFilesWritten + 1
                    else:
                        print "Skipped frame number " + str(frameNumber)
                    cv2.destroyAllWindows()
                else:
                    cv2.imwrite(targetFileName, frame)
            
        else:
            cap.release()    
    print "Done with " + sourceFileName  + ", wrote " + str(numFilesWritten) + " files"
    return baseDirName


class LabelTool():
    def __init__(self, master):
        # set up the main frame
        self.parent = master
        self.parent.title("LabelTool")
        self.frame = Frame(self.parent)
        self.frame.pack(fill=BOTH, expand=1)
        self.parent.resizable(width = FALSE, height = FALSE)

        # initialize global state
        self.imageDir = ''
        self.imageList= []
        self.egDir = ''
        self.egList = []
        self.outDir = ''
        self.cur = 0
        self.total = 0
        self.category = 0
        self.imagename = ''
        self.labelfilename = ''
        self.tkimg = None
        self.currentLabelclass = ''
        self.cla_can_temp = []
        self.classcandidate_filename = 'class.txt'

        # initialize mouse state
        self.STATE = {}
        self.STATE['click'] = 0
        self.STATE['x'], self.STATE['y'] = 0, 0

        # reference to bbox
        self.bboxIdList = []
        self.bboxId = None
        self.bboxList = []
        self.hl = None
        self.vl = None

        # ----------------- GUI stuff ---------------------
        # dir entry & load
        self.label = Label(self.frame, text = "Image Dir:")
        self.label.grid(row = 0, column = 0, sticky = E)
        self.entry = Entry(self.frame)
        self.entry.grid(row = 0, column = 1, sticky = W+E)
        self.ldBtn = Button(self.frame, text = "Load", command = self.loadDir)
        self.ldBtn.grid(row = 0, column = 2,sticky = W+E)

        # main panel for labeling
        self.mainPanel = Canvas(self.frame, cursor='tcross')
        self.mainPanel.bind("<Button-1>", self.mouseClick)
        self.mainPanel.bind("<Motion>", self.mouseMove)
        self.parent.bind("<Escape>", self.cancelBBox)  # press <Espace> to cancel current bbox
        self.parent.bind("s", self.cancelBBox)
        self.parent.bind("a", self.prevImage) # press 'a' to go backforward
        self.parent.bind("d", self.nextImage) # press 'd' to go forward
        self.mainPanel.grid(row = 1, column = 1, rowspan = 4, sticky = W+N)

        # choose class
        self.classname = StringVar()
        self.classcandidate = ttk.Combobox(self.frame,state='readonly',textvariable=self.classname)
        self.classcandidate.grid(row=1,column=2)
        if os.path.exists(self.classcandidate_filename):
            with open(self.classcandidate_filename) as cf:
                for line in cf.readlines():
                    className = line.strip('\n')
                    className = className.strip('\r')
                    print className
                    self.cla_can_temp.append(className) 
        print self.cla_can_temp
        self.classcandidate['values'] = self.cla_can_temp
        self.classcandidate.current(0)
        self.currentLabelclass = self.classcandidate.get() #init
        self.btnclass = Button(self.frame, text = 'ComfirmClass', command = self.setClass)
        self.btnclass.grid(row=2,column=2,sticky = W+E)

        # showing bbox info & delete bbox
        self.lb1 = Label(self.frame, text = 'Bounding boxes:')
        self.lb1.grid(row = 3, column = 2,  sticky = W+N)
        self.listbox = Listbox(self.frame, width = 22, height = 12)
        self.listbox.grid(row = 4, column = 2, sticky = N+S)
        self.btnDel = Button(self.frame, text = 'Delete', command = self.delBBox)
        self.btnDel.grid(row = 5, column = 2, sticky = W+E+N)
        self.btnClear = Button(self.frame, text = 'ClearAll', command = self.clearBBox)
        self.btnClear.grid(row = 6, column = 2, sticky = W+E+N)

        # control panel for image navigation
        self.ctrPanel = Frame(self.frame)
        self.ctrPanel.grid(row = 7, column = 1, columnspan = 2, sticky = W+E)
        self.prevBtn = Button(self.ctrPanel, text='<< Prev', width = 10, command = self.prevImage)
        self.prevBtn.pack(side = LEFT, padx = 5, pady = 3)
        self.nextBtn = Button(self.ctrPanel, text='Next >>', width = 10, command = self.nextImage)
        self.nextBtn.pack(side = LEFT, padx = 5, pady = 3)
        self.progLabel = Label(self.ctrPanel, text = "Progress:     /    ")
        self.progLabel.pack(side = LEFT, padx = 5)
        self.tmpLabel = Label(self.ctrPanel, text = "Go to Image No.")
        self.tmpLabel.pack(side = LEFT, padx = 5)
        self.idxEntry = Entry(self.ctrPanel, width = 5)
        self.idxEntry.pack(side = LEFT)
        self.goBtn = Button(self.ctrPanel, text = 'Go', command = self.gotoImage)
        self.goBtn.pack(side = LEFT)


        # example pannel for illustration
        self.egPanel = Frame(self.frame, border = 10)
        self.egPanel.grid(row = 1, column = 0, rowspan = 5, sticky = N)
        self.tmpLabel2 = Label(self.egPanel, text = "Examples:")
        self.tmpLabel2.pack(side = TOP, pady = 5)
        self.egLabels = []
        for i in range(3):
            self.egLabels.append(Label(self.egPanel))
            self.egLabels[-1].pack(side = TOP)

        # display mouse position
        self.disp = Label(self.ctrPanel, text='')
        self.disp.pack(side = RIGHT)

        self.frame.columnconfigure(1, weight = 1)
        self.frame.rowconfigure(4, weight = 1)

        # for debugging
##        self.setImage()
##        self.loadDir()

    def loadDir(self, dbg = False):
        deleteExistingFiles = False
        s = self.entry.get()
        self.parent.focus()
        if (s.endswith('.avi') or s.endswith('.mp4')):
            aviFileName = s
            self.baseDirName = extractFramesFromFile(s)  # The images are stored in self.baseDirName/Images
            # Delete all files in the 999 directory
            deleteExistingFiles = True
            shutil.copy(aviFileName, self.baseDirName)
                
        else:
            self.baseDirName = s
            if not os.path.exists(self.baseDirName):
                tkMessageBox.showerror("Error!", message = s + " is neither an AVI file nor a valid directory")
                return
          
        self.baseDirName = os.path.join(os.getcwd(), self.baseDirName) 
        print "Base dir  : " + self.baseDirName
        shutil.copy(self.classcandidate_filename, self.baseDirName)
        
        # get image list
        self.imageDir = os.path.join(self.baseDirName, 'Images')
        if not os.path.exists(self.imageDir):
            tkMessageBox.showerror("Error!", message = "The image directory " + self.imageDir + "does not exist!")
            return
        print "Images in : " + self.imageDir 
        
        self.imageList = glob.glob(os.path.join(self.imageDir, '*.JPG'))
        self.imageList = self.imageList + glob.glob(os.path.join(self.imageDir, '*.jpg'))
        self.imageList = self.imageList + glob.glob(os.path.join(self.imageDir, '*.png'))
        print self.imageList
        if len(self.imageList) == 0:
            print( 'No .JPG, jpg or png images found in the specified dir!')
            return

        # default to the 1st image in the collection
        self.cur = 1
        self.total = len(self.imageList)

         # set up output dir
        self.outDir = os.path.join(self.baseDirName, 'Labels')
        if not os.path.exists(self.outDir):
            os.mkdir(self.outDir)
        elif deleteExistingFiles:
            os.remove([f for f in os.listdir(self.outDir)])

        # set up YOLO output dir
        self.yoloOutDir = os.path.join(self.baseDirName, 'yoloLabels')
        if not os.path.exists(self.yoloOutDir):
            os.mkdir(self.yoloOutDir)
        elif deleteExistingFiles:
            os.remove([f for f in os.listdir(self.yoloOutDir)])
            

        # load example bboxes
        #self.egDir = os.path.join(r'./Examples', '%03d' %(self.category))
        self.egDir = os.path.join(r'./Examples/demo')
        print( os.path.exists(self.egDir))
        if not os.path.exists(self.egDir):
            return
        filelist = glob.glob(os.path.join(self.egDir, '*.JPG'))
        self.tmp = []
        self.egList = []
        random.shuffle(filelist)
        for (i, f) in enumerate(filelist):
            if i == 3:
                break
            im = Image.open(f)
            r = min(SIZE[0] / im.size[0], SIZE[1] / im.size[1])
            new_size = int(r * im.size[0]), int(r * im.size[1])
            self.tmp.append(im.resize(new_size, Image.ANTIALIAS))
            self.egList.append(ImageTk.PhotoImage(self.tmp[-1]))
            self.egLabels[i].config(image = self.egList[-1], width = SIZE[0], height = SIZE[1])

        self.loadImage()
        print( '%d images loaded from %s' %(self.total, s))

    def loadImage(self):
        # load image
        imagepath = self.imageList[self.cur - 1]
        self.img = Image.open(imagepath)
        self.tkimg = ImageTk.PhotoImage(self.img)
        self.mainPanel.config(width = max(self.tkimg.width(), 400), height = max(self.tkimg.height(), 400))
        self.mainPanel.create_image(0, 0, image = self.tkimg, anchor=NW)
        self.progLabel.config(text = "%04d/%04d" %(self.cur, self.total))

        # load labels
        self.clearBBox()
        self.imagename = os.path.split(imagepath)[-1].split('.')[0]
        labelname = self.imagename + '.txt'
        self.labelfilename = os.path.join(self.outDir, labelname)
        self.yolofilename  = os.path.join(self.yoloOutDir, labelname)

        bbox_cnt = 0
        if os.path.exists(self.labelfilename):
            with open(self.labelfilename) as f:
                for (i, line) in enumerate(f):
                    if i == 0:
                        bbox_cnt = int(line.strip())
                        continue
                    # tmp = [int(t.strip()) for t in line.split()]
                    tmp = line.split()
                    #print tmp
                    self.bboxList.append(tuple(tmp))
                    tmpId = self.mainPanel.create_rectangle(int(tmp[0]), int(tmp[1]), \
                                                            int(tmp[2]), int(tmp[3]), \
                                                            width = 2, \
                                                            outline = COLORS[(len(self.bboxList)-1) % len(COLORS)])
                    # print tmpId
                    self.bboxIdList.append(tmpId)
                    self.listbox.insert(END, '%s : (%d, %d) -> (%d, %d)' %(tmp[4],int(tmp[0]), int(tmp[1]), \
                    												  int(tmp[2]), int(tmp[3])))
                    self.listbox.itemconfig(len(self.bboxIdList) - 1, fg = COLORS[(len(self.bboxIdList) - 1) % len(COLORS)])

    def saveImage(self):
        yf = open(self.yolofilename, 'w')
        with open(self.labelfilename, 'w') as f:
            f.write('%d\n' %len(self.bboxList))
            imgWidth = self.tkimg.width()
            imgHeight= self.tkimg.height()
            print "------" + self.labelfilename + "------"
            print imgWidth
            print imgHeight
            print self.cla_can_temp
            for bbox in self.bboxList:
                f.write(' '.join(map(str, bbox)) + '\n')
                print bbox
                # (left, top, right, bottom, className) -> [classNumber, center_x / W, center_y / H, width / W, height / H]
                (left, top, right, bottom,currClassName) = bbox
                center_x = (float(left)  + float(right)) / 2
                center_y = (float(top)   + float(bottom)) / 2
                width    = float(right)  - float(left)
                height   = float(bottom) - float(top)
                currClassNameLen = len(currClassName)
                for idx, className in enumerate(self.cla_can_temp):
                    if className[:currClassNameLen] == currClassName:
                        currClassIdx = idx
                        break
                printStr = '%d %f %f %f %f\n' %(currClassIdx, center_x / imgWidth, center_y / imgHeight, \
                                                width / imgWidth, height / imgHeight)
                yf.write(printStr)
                print printStr
                
        print( 'Image No. %d saved' %(self.cur))
        yf.close()

    def mouseClick(self, event):
        if self.STATE['click'] == 0:
            self.STATE['x'], self.STATE['y'] = event.x, event.y
        else:
            x1, x2 = min(self.STATE['x'], event.x), max(self.STATE['x'], event.x)
            y1, y2 = min(self.STATE['y'], event.y), max(self.STATE['y'], event.y)
            self.bboxList.append((x1, y1, x2, y2, self.currentLabelclass))
            self.bboxIdList.append(self.bboxId)
            self.bboxId = None
            self.listbox.insert(END, '%s : (%d, %d) -> (%d, %d)' %(self.currentLabelclass,x1, y1, x2, y2))
            self.listbox.itemconfig(len(self.bboxIdList) - 1, fg = COLORS[(len(self.bboxIdList) - 1) % len(COLORS)])
        self.STATE['click'] = 1 - self.STATE['click']

    def mouseMove(self, event):
        self.disp.config(text = 'x: %d, y: %d' %(event.x, event.y))
        if self.tkimg:
            if self.hl:
                self.mainPanel.delete(self.hl)
            self.hl = self.mainPanel.create_line(0, event.y, self.tkimg.width(), event.y, width = 2)
            if self.vl:
                self.mainPanel.delete(self.vl)
            self.vl = self.mainPanel.create_line(event.x, 0, event.x, self.tkimg.height(), width = 2)
        if 1 == self.STATE['click']:
            if self.bboxId:
                self.mainPanel.delete(self.bboxId)
            self.bboxId = self.mainPanel.create_rectangle(self.STATE['x'], self.STATE['y'], \
                                                            event.x, event.y, \
                                                            width = 2, \
                                                            outline = COLORS[len(self.bboxList) % len(COLORS)])

    def cancelBBox(self, event):
        if 1 == self.STATE['click']:
            if self.bboxId:
                self.mainPanel.delete(self.bboxId)
                self.bboxId = None
                self.STATE['click'] = 0

    def delBBox(self):
        sel = self.listbox.curselection()
        if len(sel) != 1 :
            return
        idx = int(sel[0])
        self.mainPanel.delete(self.bboxIdList[idx])
        self.bboxIdList.pop(idx)
        self.bboxList.pop(idx)
        self.listbox.delete(idx)

    def clearBBox(self):
        for idx in range(len(self.bboxIdList)):
            self.mainPanel.delete(self.bboxIdList[idx])
        self.listbox.delete(0, len(self.bboxList))
        self.bboxIdList = []
        self.bboxList = []

    def prevImage(self, event = None):
        self.saveImage()
        if self.cur > 1:
            self.cur -= 1
            self.loadImage()

    def nextImage(self, event = None):
        self.saveImage()
        if self.cur < self.total:
            self.cur += 1
            self.loadImage()

    def gotoImage(self):
        idx = int(self.idxEntry.get())
        if 1 <= idx and idx <= self.total:
            self.saveImage()
            self.cur = idx
            self.loadImage()

    def setClass(self):
    	self.currentLabelclass = self.classcandidate.get()
    	print ('set label class to :',self.currentLabelclass)

##    def setImage(self, imagepath = r'test2.png'):
##        self.img = Image.open(imagepath)
##        self.tkimg = ImageTk.PhotoImage(self.img)
##        self.mainPanel.config(width = self.tkimg.width())
##        self.mainPanel.config(height = self.tkimg.height())
##        self.mainPanel.create_image(0, 0, image = self.tkimg, anchor=NW)

if __name__ == '__main__':
    root = Tk()
    tool = LabelTool(root)
    root.resizable(width =  True, height = True)
    root.mainloop()
