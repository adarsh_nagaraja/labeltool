### Forked from [puzzledqs/BBox-Label-Tool](https://github.com/puzzledqs/BBox-Label-Tool)
## Improvements
1. Add multi-class support 
2. Change some of the color-candidates for better display
3. Fix the 'Example' filepath for convenience
4. Change the image format from '.JPEG' to '.JPG'

## New Usage
### For multi-class task, modify 'class.txt' with your own class-candidates and before labeling bbox, choose the 'Current Class' in the Combobox and make sure you click 'ComfirmClass' button.

### The remaining usage is the same as the origin one.

------------------------------------

**Contact info**: jxgu1016@gmail.com

------------------------------------

BBox-Label-Tool
===============

A simple tool for labeling object bounding boxes in images, implemented with Python Tkinter.

Data Organization
-----------------
LabelTool  
|  
|--main.py   *# source code for the tool*  
|  
----------
python 2.7 win 32bit
PIL-1.1.7.win32-py2.7

Usage
-----
Input is either an avi file <filePath/fileName.avi> or a dirPath

1. If input is an avi file, a directory by name <fileName> is created. A tool is called to extract frames (totalling 2 fps) in the sub-directory dirPath/Images. If there are more than 30 images (i.e., if the video is more than 15 seconds long), the user is prompted on each image whether to save y, no n, or quit q. If not, the code changes if <dirPath/Images> is a valid direcotry with jpg, JPG or png files

2. Files are loaded one by one. 
$ python main.py
a. The images along with a few example results will be loaded.
b. To create a new bounding box, left-click to select the first vertex. Moving the mouse to draw a rectangle, and left-click again to select the second vertex.
  - To cancel the bounding box while drawing, just press <Esc>.
  - To delete a existing bounding box, select it from the listbox, and click 'Delete'.
  - To delete all existing bounding boxes in the image, simply click 'ClearAll'.
c. After finishing one image, click 'Next' to advance. Likewise, click 'Prev' to reverse. Or, input the index and click 'Go' to navigate to an arbitrary image.
  - The labeling result will be saved if and only if the 'Next' button is clicked.

This creates label files in the Labels and yoloLabels subdirectories of <fileName>, and images are also stored there. 

3. Push resultant files to amazon drive



