import os
from os import walk, getcwd
from PIL import Image
classes = ["emptyhands"]
cls_id = 1;

mypath = "emptyhands"
wd = getcwd()
list_file = open('%s\\%s\\%s_list.txt'%(wd, mypath,mypath), 'r')
imglist=list_file.read().split('\n')

def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)
    
for txt_name in imglist:
    txt_outpath = os.path.splitext(txt_name)[0] + ".txt"
    #txt_outfile = open(txt_outpath, "ab+")
    txt_outfile = open(txt_outpath, "w")    
	#txt_outfile.write(str(cls_id) + " " + str(0.5) + " " + str(0.5) + " " + str(1.0) + " " + str(1.0))
    w= 100
    h= 100
    xmin = 0;#elems[0]
    xmax = w;#elems[2]
    ymin = 0
    ymax = h
	
    print('****',xmin, xmax, ymin, ymax)
    b = (float(xmin), float(xmax), float(ymin), float(ymax))
    bb = convert((w,h), b)
    print(bb)
    txt_outfile.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')	
list_file.close() 