#!/bin/bash
# Example usage ./gatherTrainingData /home/badri/Videos/badri008.avi 002
fullVideoFileName=$1
videoFileNameOnly=$(echo `basename $fullVideoFileName` | cut -f 1 -d '.')
targetDirName=$videoFileNameOnly
echo $targetDirName
if ! [ -d $targetDirName ]; then
    echo "Creating directory " $targetDirName
    mkdir $targetDirName
    mkdir $targetDirName/Images
    mkdir $targetDirName/Labels
    mkdir $targetDirName/yoloLabels
fi

genData_DirName=$2
mv $fullVideoFileName $targetDirName
cp class.txt $targetDirName

nImgs1=$(ls ./Images/$genData_DirName/*.jpg | wc -l)
nImgs2=$(ls ./Images/$genData_DirName/*.JPG | wc -l)
nImgs3=$(ls ./Images/$genData_DirName/*.png | wc -l)
nImgs=$(($nImgs1 + $nImgs2 + $nImgs3))
echo "Number of image files : " $nImgs
nTxtFiles=$(ls ./yoloLabels/$genData_DirName/*.txt | wc -l)
echo "Number of label files : " $nTxtFiles


cp ./Images/$genData_DirName/*.* $targetDirName/Images
cp ./Labels/$genData_DirName/*.* $targetDirName/Labels
cp ./yoloLabels/$genData_DirName/*.* $targetDirName/yoloLabels

