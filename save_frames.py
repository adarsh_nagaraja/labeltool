import os
import sys
import json
import subprocess
import cv2
import time

'''
USAGE:

python  save_frames.py <videoFileName.avi> "-i"  opens the videof ile extracts frames from it at 2 fps and saves each frame. 
                        The output directory name is is Images/xxx where xxx is the first non-existent directory from 000 to 999
python save_frames.py <camera> "-i" opens up camera 0 and records images at a target rate of 2 fps. All outputs are stored
                        in captures_<timeAndData>/Images/filename
The "-i" option, if specified causes the code to pause fater each file is created
'''

def main(argv):
    separator     = '/'

    sourceFileName = argv[0]
    if sourceFileName == 'camera':
        cap = cv2.VideoCapture(0)
        rawSourceFileName = 'cam'
        targetFrameRate  = 0.5            # 0.5 fps is plenty
        targetDirName = 'captures_' + time.strftime('%m-%d-%Y-%H-%M-%S', time.localtime(time.time()))
        os.mkdir(targetDirName)
        targetDirName = os.path.join(targetDirName, 'Images')
        os.mkdir(targetDirName)
         
    else:  
        cap = cv2.VideoCapture(sourceFileName)
        rawSourceFileName = os.path.split(sourceFileName)[-1].split('.')[0]
        targetFrameRate  = 0.5            # 2 fps is plenty
        for ii in xrange(10000):
            targetDirName = os.path.join(os.getcwd(), 'Images', "%03d"%ii)
            if os.path.exists(targetDirName):
                pass
            else:
                os.mkdir(targetDirName) 
                break
    
    print "Saving output to " + targetDirName
    inputFrameRate   = cap.get(5)
    print rawSourceFileName

    interactiveFlag = False
    if len(argv) > 1:
        if argv[1] == '-i':
            interactiveFlag = True

    
    downSamplingRate = max(int(inputFrameRate / targetFrameRate), 1)
    frameNumber      = 0
    lastCaptureTime  = 0.0
    numFilesWritten  = 0
    continueFlag     = True
    downSamplingRate = 1
    while cap.isOpened() and continueFlag:
        ret, frame = cap.read()
        if ret:
            frameNumber= frameNumber + 1
            
            if sourceFileName == 'camera':
                nextFrameFlag = (time.time() - lastCaptureTime) > (1.0 / targetFrameRate)
            else:
                nextFrameFlag = (frameNumber%downSamplingRate == 0)
                
            if nextFrameFlag:
                lastCaptureTime = time.time()
                targetFileName = os.path.join(targetDirName, rawSourceFileName +  '_frame' + str(frameNumber) + '.jpg')
                if interactiveFlag:
                    cv2.imshow('Press y(n) to save (discard) frame_' + str(frameNumber), frame)
                    userInput = cv2.waitKey(0) & 0xff 
                    if userInput == ord('q'):
                        continueFlag = False
                    elif userInput == ord('y'):
                        saveFlag = True
                        cv2.imwrite(targetFileName, frame)
                        print "Wrote frame number "   + str(frameNumber) + " to " + targetFileName
                        numFilesWritten = numFilesWritten + 1
                    else:
                        print "Skipped frame number " + str(frameNumber)
                    cv2.destroyAllWindows()
                else:
                    cv2.imshow('Test window', frame)
                    cv2.waitKey(1)
                    print targetFileName
                    cv2.imwrite(targetFileName, frame)
            
        else:
            cap.release()    
    print "Done with " + sourceFileName  + ", wrote " + str(numFilesWritten) + " files"

if __name__ == '__main__': 
    main(sys.argv[1:])

